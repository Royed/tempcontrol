# README #

A easy to use UI component for Temperature Control. There is a handle that is moveable, and a indicator that shows the current reading. 

![screenShot](https://s31.postimg.org/j3093m48r/Screen_Shot_2016_07_13_at_4_11_34_PM.png)

### Temperature Conrol ###

* V1.1
* Language (Swift 3.0)


### How do I get set up? ###
There is a example application that describes how to use the component. 

* Copy the folder 'tempDial'
* Take story board, make a UIVIew,
* Set class of view as 'tempDialView'
* Create link of tempDialView to the view controller by creating an IBOutlet
* Run the application to see the temp control in action
* Use **tempDialViewDelegate**, and its method **temperatureUpdated** to know when set temperature is updated by user.
* Please refer the example code, to understand how to set minAngle, maxAngle, minTemp, MaxTemperature, currentTemperature.
