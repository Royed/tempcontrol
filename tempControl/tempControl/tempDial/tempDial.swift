//
//  tempDial.swift
//  tempControl
//
//  Created by Roy on 25/04/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import UIKit
protocol tempDialViewDelegate: class {
    func temperatureUpdated(_ sender:tempDialView, updatedTemp:Float)
}

@IBDesignable class dialView: UIView {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var handleView: UIView!
    
    @IBOutlet internal var sqView: UIView!
    @IBOutlet internal var dialBgImgView: UIImageView!
    @IBOutlet internal var valueLabel: UILabel!
    @IBOutlet internal var indicatorImageView: UIImageView!
    
    @IBOutlet internal var handleView_label: UILabel!
    
    @IBOutlet var handleView_y: NSLayoutConstraint!
    @IBOutlet var handleView_x: NSLayoutConstraint!
    
    var shouldAnimate : Bool = false
    
    let refAngle : Float = 180
    
    var minAngle : Float = 70
    var maxAngle : Float = 290

    fileprivate var touchedInsideHandle : Bool = false
    
    fileprivate var _handle_value : Float = 0
    fileprivate var _dispValue : Float = 0
    fileprivate var _isobservingBounds = false
    
    var handle_value : Float {
        get { return _handle_value }
        set {
            if(newValue < minValue){
                _handle_value = minValue
            }
            else if(newValue >= maxValue){
                _handle_value = maxValue
            }
            else{
                _handle_value = newValue
            }
            updateHandle()
        }
    }
    
    @IBInspectable  var bgImage : UIImage? = UIImage(named: "tempDial_dial"){
        didSet{
            dialBgImgView.image = bgImage
        }
    }
    
    
    @IBInspectable internal var dispValue : Float {
        get { return _dispValue }
        set {
            if(newValue < minValue){
                _dispValue = minValue
            }
            else if(newValue >= maxValue){
                _dispValue = maxValue
            }
            else{
                _dispValue = newValue
            }
            updateUI()
        }
    }
    
    @IBInspectable var minValue : Float = 0.0{
        didSet{
            if(dispValue < minValue) {
                dispValue = minValue;
            }
            if(handle_value < minValue) {
                handle_value = minValue
            }
        }
    }
    
    @IBInspectable var maxValue : Float = 30.0{
        didSet{
            if(dispValue > maxValue)
            {
                dispValue = maxValue;
            }
            if(handle_value > maxValue) {
                handle_value = maxValue
            }
        }
    }
    
    //MARK:Lifecycle Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewForXib()
    }
        
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViewForXib()
    }
    
    func loadFromNib() ->UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "tempDial", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
    override func draw(_ rect: CGRect) {
        //Update drawing
        updateUI()
    }
    
    deinit
    {
        removeBoundObserver()
    }
    
    //MARK: UI
    func setupViewForXib(){
        containerView = loadFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(containerView)
    }
    
    func autoRepondToFrameChange(_ autoRerspond: Bool)
    {
        removeBoundObserver() //we dont want multiple obervers, so we remove the current oberver if it exists
        if(autoRerspond){
            sqView.layer.addObserver(self, forKeyPath: "bounds", options: NSKeyValueObservingOptions.new, context: nil)
            _isobservingBounds = true

        }
    }
    
    fileprivate func removeBoundObserver() {
        //Replace with try catch if possible. _isobservingBounds is a bad method
        if(_isobservingBounds){
            removeObserver(self, forKeyPath: "bounds")
            _isobservingBounds = false
        }
//        do{ try removeObserver(self, forKeyPath: "bounds")}
    }
    fileprivate func placeHandleViewForValue(_ value: Float) {
        let angle = degForValue(value)
        placeHandleViewForAngle(angle)
    }
    
    fileprivate func placeHandleViewForAngle(_ deg:Float) {
    let r = getHandleRadius()
    let angle_rad = Float(deg2rad(Double(deg - 90.0)))
    
    let x = r * cos(angle_rad)
    let y = r * sin(angle_rad)
    
    handleView_x.constant = CGFloat(x)
    handleView_y.constant = CGFloat(y)
    }
    
    func updateDisplayLabel() {
        valueLabel.text = String(format: "%0.1f", dispValue)
    }
    
    func updateDisplayLabel(_ val:Float) {
        valueLabel.text = String(format: "%0.1f", val)
    }
    
    func updateUI(){
        updateDisplayLabel()
        updateIndicator(shouldAnimate)
        updateHandle()
    }
    
    func getHandleRadius() -> Float
    {
        return Float(sqView.bounds.width / 2.0) * 0.65
    }
    
    func getCenterPoint() -> CGPoint
    {
        layoutIfNeeded()
        var cntrPoint = CGPoint.zero
        cntrPoint.x = sqView.bounds.midX
        cntrPoint.y = sqView.bounds.midY
        return cntrPoint
    }
    
    func updateIndicator(_ anmated : Bool = false)
    {
        let deg = Double(degForValue(dispValue))
        let rad = deg2rad(deg)
        if(anmated) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.indicatorImageView.transform = CGAffineTransform(rotationAngle: rad)
            })
        } else {
            indicatorImageView.transform = CGAffineTransform(rotationAngle: rad)
        }
    }
    
    func updateHandle()
    {
        placeHandleViewForValue(_handle_value)
        handleView_label.text = String(format: "%0.1f", _handle_value)
    }
    
    fileprivate func updateHandle(_ touchPoint:CGPoint)
    {
        let angle = Float(getAngle(touchPoint))
        let valFromTouch   = valueForDeg(angle)
        handle_value = valFromTouch
    }
    
    //MARK: Touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        touchedInsideHandle = false
        let touch = touches.first
        let touchLocation = touch!.location(in: self.handleView)
        if(handleView.bounds.contains(touchLocation)) {
            touchedInsideHandle = true
            updateDisplayLabel(handle_value)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        touchedInsideHandle = false
        updateUI()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if(touchedInsideHandle == true) {
            let touch = touches.first
            let touchLocation = touch!.location(in: self)
            updateHandle(touchLocation)
            updateDisplayLabel(handle_value)
        }
    }
    
    //MARK: Observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?,
        context: UnsafeMutableRawPointer?) {
        if let _ = keyPath , let lyr = object {
            if(keyPath! == "bounds" && lyr as! CALayer == sqView.layer) {
                updateUI()
            }
        }
    }
    
    //MARK: Calcualtions
    func deg2rad(_ deg:Double) -> CGFloat
    {
        return CGFloat(deg * M_PI / 180)
    }
    
    fileprivate func valuePerDegree() -> Float
    {
        let diffAngle = maxAngle - minAngle
        let diffValue = maxValue - minValue
        let vPerD : Float = diffValue / diffAngle
        return vPerD
    }
    
    fileprivate func degPerValue() -> Float
    {
        let diffAngle = maxAngle - minAngle
        let diffVal   = maxValue - minValue
        let dPerV = diffAngle / diffVal
        return dPerV
    }
    
    fileprivate func degForValue(_ val:Float) -> Float
    {
        let dPerV = degPerValue()
        let adjustedVal = (val >= minValue) ? (val - minValue) : minValue
        let deg = ((adjustedVal * dPerV) + minAngle) + refAngle
        return deg
    }
    
    fileprivate func valueForDeg(_ deg:Float) -> Float
    {
        let dPerV = degPerValue()
        var adjustedDeg = (deg - refAngle)
        if(adjustedDeg < 0) {
            adjustedDeg += 360
        }
        adjustedDeg -= minAngle
        
        let val = adjustedDeg / dPerV + minValue;
        return val
    }
    func getAngle(_ touchPoint:CGPoint) -> Double {
        
        let x = Double ( touchPoint.x - (self.bounds.width / 2))
        let y = Double ( self.bounds.height  - touchPoint.y - (self.bounds.height / 2))
        
        var angle : Double = 0
        switch (getQuadrant(x, y: y)) {
            
        case 1:
            angle = asin(y / hypot(x, y)) * 180 / M_PI;
        case 2:
            angle = 180 - asin(y / hypot(x, y)) * 180 / M_PI
        case 3:
            angle = 180 + (-1 * asin(y / hypot(x, y)) * 180 / M_PI)
        case 4:
            angle = 360 + asin(y / hypot(x, y)) * 180 / M_PI
        default:
            angle = 0
        }
        
        angle = (360 - angle) + 90
        angle = (angle > 360) ? angle - 360 : angle
        
        return angle
    }
    
    func getQuadrant(_ x:Double, y:Double) -> Int {
        if (x >= 0) {
            return y >= 0 ? 1 : 4;
        } else {
            return y >= 0 ? 2 : 3;
        }
    }
}

@IBDesignable class tempDialView: dialView
{
    
    weak var delegate:tempDialViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    fileprivate var currVal = Float(0.0) //For getting temp change
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable var minTemperature : Float{
        get{ return super.minValue }
        set{ super.minValue = newValue }
    }
    
    @IBInspectable var maxTemperature : Float{
        get{ return super.maxValue }
        set{ super.maxValue = newValue }
    }
    
    @IBInspectable var currentTemperature : Float {
        get { return super.dispValue }
        set { super.dispValue = newValue }
    }
    
    //MARK: Touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        currVal = handle_value
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
//        if let _ = delegate{
//            if(currVal != handle_value) {
//                delegate?.temperatureUpdated(self, updatedTemp: handle_value)
//            }
//        }
        if(currVal != handle_value) {
            delegate?.temperatureUpdated(self, updatedTemp: handle_value)
        }
    }
}
