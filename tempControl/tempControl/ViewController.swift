//
//  ViewController.swift
//  tempControl
//
//  Created by Roy on 25/04/16.
//  Copyright © 2016 InApp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, tempDialViewDelegate{

    @IBOutlet var temperatureDial: tempDialView!
    @IBOutlet var minValTextField: UITextField!
    @IBOutlet var maxValTextfield: UITextField!
    @IBOutlet var currTempTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        temperatureDial.shouldAnimate = true
        
        temperatureDial.minTemperature     = 15.0
        temperatureDial.maxTemperature     = 35.0
        temperatureDial.currentTemperature = 20.0
        
        temperatureDial.handle_value = 0
        
        temperatureDial.updateUI()
        updateValues()
        
        temperatureDial.autoRepondToFrameChange(true)
        
        temperatureDial.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateBtnAction()
    {
        temperatureDial.minTemperature = (minValTextField.text?.floatValue)!
        temperatureDial.maxTemperature = (maxValTextfield.text?.floatValue)!
        temperatureDial.currentTemperature = (currTempTextfield.text?.floatValue)!
        updateValues()
    }
    
    func updateValues()
    {
        minValTextField.text = String(stringInterpolationSegment: temperatureDial.minTemperature)
        maxValTextfield.text = String(stringInterpolationSegment: temperatureDial.maxTemperature)
        currTempTextfield.text = String(stringInterpolationSegment: temperatureDial.currentTemperature)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    //MARK:tempDialViewDelegate
    func temperatureUpdated(_ sender: tempDialView, updatedTemp: Float) {
        print("Updated Temp = ", updatedTemp)
    }
}


extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
